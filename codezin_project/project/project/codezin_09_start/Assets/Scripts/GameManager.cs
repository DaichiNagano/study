﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    private GameObject _charactor;
    private CharactorManager _charactorScript;

    // Use this for initialization
    void Start () {
        // キャラクターを取得する
        this._charactor = GameObject.Find("charactor");

        // キャラクターのスクリプトを取得する
        this._charactorScript = this._charactor.GetComponent<CharactorManager>();
    }

    // 左右の移動にかかる力
    private const float _horizontalForce = 15f;

    // ジャンプ時にかかる力
    private const float _upForce = 800f;

    // Update is called once per frame
    void Update () {

        // 左右のキーで左右に移動
        float horizontal = Input.GetAxis("Horizontal");

        // キー入力がありジャンプ中でない場合は左右に移動
        if (horizontal != 0.0f && this._charactorScript.isJumping == false)
        {
            this._moveCharactor(Vector2.right * _horizontalForce * horizontal);
        }
        // スペースキーでジャンプ
        // ジャンプ中はスペースキーが押されてもさらに上昇しない
        if (Input.GetKeyDown("space") && this._charactorScript.isJumping == false)
        {
            this._moveCharactor(Vector2.up * _upForce);
        }
    }

    /// <summary>
    /// キャラクターを移動させる
    /// </summary>
    /// <param name="force">移動させる方向と値</param>
    private void _moveCharactor(Vector2 force)
    {
        this._charactor.rigidbody2D.AddForce(force);
    }
}
