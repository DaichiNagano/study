﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharactorManager : MonoBehaviour {

    // ジャンプしているかどうか
    private bool _isJumping;

    // 左向きかどうか
    private bool _isLeftMove;

    // 停止しているかどうか
    private bool _isStop;

    // アニメーション制御用のAnimatorクラス
    private Animator _animator;

    public bool isJumping
    {
        get { return _isJumping; }
    }

    // Use this for initialization
    void Start () {
        this._animator = this.GetComponent<Animator>();

        this._isLeftMove = true;
        this._isJumping = false;
        this._isStop = true;
    }

    // Update is called once per frame
    void Update () {
        // 自分の速度を取得
        var v = this.rigidbody2D.velocity;

        // 縦方向の速度によってフラグを調整
        // 0.1ずつずらしてあるのは誤差対応のため
        if (v.x < -0.1f)
        {
            this._isLeftMove = true;
            this._isStop = false;
        }
        else if (v.x > 0.1f)
        {
            this._isLeftMove = false;
            this._isStop = false;
        }
        else
        {
            this._isStop = true;
        }

        // Animatorのパラメーターを変更
        this._animator.SetBool("isJump", this._isJumping);
        this._animator.SetBool("isStop", this._isStop);
        this._animator.SetBool("isLeftMove", this._isLeftMove);
    }

    // キャラクターに接触しているGameObjectのリスト
    private List<GameObject> _collisionList = new List<GameObject>();

    /// <summary>
    /// 接触した（=着地）
    /// </summary>
    /// <param name="col"></param>
    public void OnCollisionEnter2D(Collision2D col)
    {
        this._collisionList.Add(col.gameObject);

        this._isJumping = false;
    }

    /// <summary>
    /// 接触状態から離れた（=ジャンプ開始）
    /// </summary>
    /// <param name="col"></param>
    public void OnCollisionExit2D(Collision2D col)
    {
        this._collisionList.Remove(col.gameObject);

        // 接触しているGameObjectが0の場合はジャンプしていると判定してアニメーションを開始する
        if (this._collisionList.Count == 0)
        {
            this._isJumping = true;
        }
    }
}
