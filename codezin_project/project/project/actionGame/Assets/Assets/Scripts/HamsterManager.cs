﻿using UnityEngine;
using System.Collections;

public class HamsterManager : MonoBehaviour {
    //左向きかどうか
    private bool _isLeftMove;

    private float _movePoint = 0.1f;

    private GameObject _hamster;

    private Animator _animator;

	// Use this for initialization
	void Start () {

        this._isLeftMove = true;
        this._hamster = GameObject.Find("hamster");
        this._animator = this._hamster.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        var position = this.transform.position;

        if (this._isLeftMove)
        {
            position.x -= this._movePoint;
        }
        else
        {
            position.x += this._movePoint;
        }

        transform.position = position;
	}
    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "leftHitArea")
        {
            this._isLeftMove = false;
            _animator.Play("hamster_move_right");

        }
        else if (col.gameObject.name == "rightHitArea")
        {
            this._isLeftMove = true;
            _animator.Play("hamster_move_left");
        }
    }
    public void OnCollisionStay2D(Collision2D col)
    {
        if (Input.GetKeyDown("space") && col.gameObject.name == "floor1")
        {
            //this._movePoint += 0.1f;
        }
        //if(col.gameObject.name == "floor1" && this._movePoint > 0.1f)
        //{
        //    this._movePoint -= 0.001f;
        //}
    }
}
