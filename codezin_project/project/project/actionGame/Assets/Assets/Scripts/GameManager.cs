﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private GameObject _charactor;
    private CharactorManager _charactorScript;
    //左右の移動にかかる力
    private float _horizontalForce = 15f;
    //ジャンプ時にかかる力
    private float _upForce = 800f;

    private DateTime _startTime;

    //ゲームの制限時間
    private readonly int _limitTime = 10;

    // Use this for initialization
    void Start()
    {
        // キャラクターを取得する
        this._charactor = GameObject.Find("charactor");

        //キャラクターのスクリプトを取得する
        this._charactorScript = this._charactor.GetComponent<CharactorManager>();

        //ゲームの開始時刻を取得
        this._startTime = DateTime.Now;

        //タイムアウトは非表示に
        GameObject.Find("time_out").GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // キーの入力方向を取得する
        float horizontal = Input.GetAxis("Horizontal");

        //キー入力がありジャンプ中でない場合に左右に移動
        if(horizontal != 0.0f && this._charactorScript.isJumping == false)
        {
            this._moveCharactor(Vector2.right * horizontal * _horizontalForce);
        }

        // キャラクターに加える力にキーの方向を反映させる
        //this._moveCharactor(Vector2.right * 10f * horizontal);

        if (Input.GetKeyDown("space") && this._charactorScript.isJumping == false)
        {
            this._moveCharactor(Vector2.up * _upForce);
        }

        //経過時間を取得
        TimeSpan pastTime = DateTime.Now - this._startTime;

        //カウントダウンを開始
        var CountDown = this._limitTime - pastTime.Seconds;

        if (CountDown >= 0)
        {
            //文字を変更
            Text countDownText = GameObject.Find("countDownText").GetComponent<Text>();
            countDownText.text = CountDown.ToString();
        }
        else
        {
            GameObject.Find("time_out").GetComponent<Renderer>().enabled = true;
            GameObject.Find("countDownText").GetComponent<Text>().text = "";
        }
    }
    /// <summary>
    /// キャラクターを移動させる
    /// </summary>
    /// <param name="force">移動させる方向と値</param>
    private void _moveCharactor(Vector2 force)
    {
        this._charactor.GetComponent<Rigidbody2D>().AddForce(force);
    }
}