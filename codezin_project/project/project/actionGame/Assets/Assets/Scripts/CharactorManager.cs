﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharactorManager : MonoBehaviour {
    //ジャンプしているかどうか
    private bool _isJumping;
    //左向きかどうか
    private bool _isLeftMove;
    //停止しているかどうか
    private bool _isStop;
    //アニメーション制御用のanimatorクラス
    private Animator _animator;

    private List<GameObject> _collisionList = new List<GameObject>();

    public bool isJumping
    {
        get { return _isJumping; }
    }

	// Use this for initialization
	void Start () {
        this._animator = this.GetComponent<Animator>();

        this._isLeftMove = true;
        this._isJumping = false;
        this._isStop = true;
	}
	
	// Update is called once per frame
	void Update () {
        //自分の速度を取得
        var v = this.GetComponent<Rigidbody2D>().velocity;

        //縦方向の速度によってフラグを調整
        //0.1ずつずらしてあるのは誤差対応のため
        if(v.x < -0.1f)
        {
            this._isLeftMove = true;
            this._isStop = false;
        }
        else if(v.x > 0.1f)
        {
            this._isLeftMove = false;
            this._isStop = false;
        }
        else
        {
            this._isStop = true;
        }

        //Animatorのパラメータを変更
        this._animator.SetBool("isJump", this._isJumping);
        this._animator.SetBool("isStop", this._isStop);
        this._animator.SetBool("isLeftMove", this._isLeftMove);
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "hamster")
        {
            this._collisionList.Remove(col.gameObject);
            Destroy(col.gameObject);
        }
        else if(col.gameObject.name=="ground" || col.gameObject.name == "floor")
        {
            this._collisionList.Add(col.gameObject);
            this._isJumping = false;
        } 
    }
    public void OnCollisionExit2D(Collision2D col)
    {
        if(col.gameObject.name=="ground" || col.gameObject.name == "floor") {  
            this._collisionList.Remove(col.gameObject);
            if (this._collisionList.Count == 0) { 
                this._isJumping = true;
            }
        }
    }
}
